<?php
namespace Keepper\SmartHouseMegadBridge\Tests\OneWire;

use Keepper\MegaD\Transport\MegaDeviceTransport;
use Keepper\SmartHouseMegadBridge\OneWire\TemperatureSensor;

class TemperatureSensorTest extends \PHPUnit_Framework_TestCase {

    /**
     * @group integration
     * @dataProvider dataProviderForTemperature
     */
    public function testGetTemperature($port, $uuid) {
        $sensor = new TemperatureSensor($port, new MegaDeviceTransport(), $uuid);
        $sensor->getValue();
    }

    public function dataProviderForTemperature() {
        return [
            [0, 'ff563e501704'],
            [0, 'ffae58501704']
        ];
    }
}
<?php
namespace Keepper\SmartHouseMegadBridge\Tests\OneWire;

use Keepper\MegaD\Transport\MegaDeviceTransport;
use Keepper\SmartHouseMegadBridge\OneWire\LabelSensor;
use Keepper\SmartHouseMegadBridge\OneWire\TemperatureSensor;

class LabelSensorTest extends \PHPUnit_Framework_TestCase {

    /**
     * @group integration
     * @dataProvider dataProviderForState
     */
    public function testGetState($port, $uuid, $expected) {
        $sensor = new LabelSensor($port, new MegaDeviceTransport(), $uuid);
        $this->assertEquals($expected, $sensor->getValue());
    }

    public function dataProviderForState() {
        return [
            [0, 'ff563e501704', true],
            [0, 'ffae58501704', true],
            [0, 'ffffffffffff', false],
        ];
    }
}
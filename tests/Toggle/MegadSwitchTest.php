<?php
namespace Keepper\SmartHouseMegadBridge\Tests\Toggle;

use Keepper\MegaD\Transport\MegaDeviceTransport;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Keepper\SmartHouseMegadBridge\Toggle\MegadSwitch;
use Psr\Http\Message\ResponseInterface;

class MegadSwitchTest extends \PHPUnit_Framework_TestCase {

    /**
     * @group integration
     */
    public function testState() {
        $transport = new MegaDeviceTransport();
        $switch = new MegadSwitch(22, $transport);
        $this->assertFalse($switch->state());
    }

    /**
     * @dataProvider dataProviderForTurns
     */
    public function testTurnOn($method, $expectedCmd, $defState) {
        $transport = $this->getMockBuilder(MegaDeviceTransportInterface::class)->getMock();
        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $response->method('getStatusCode')->willReturn(200);

        $transport
            ->expects($this->once())
            ->method('request')
            ->with(['pt' => 22, 'cmd' => $expectedCmd])
            ->willReturn($response);

        $switch = new MegadSwitch(22, $transport, $defState);
        $switch->$method();
    }

    public function dataProviderForTurns() {
        return [
            ['turnOn', '22:1', false],
            ['turnOff', '22:0', true],
            ['toggle', '22:2', false]
        ];
    }

}
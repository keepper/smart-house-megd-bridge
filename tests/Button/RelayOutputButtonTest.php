<?php
namespace Keepper\SmartHouseMegadBridge\Tests\Button;

use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Keepper\SmartHouseMegadBridge\Button\RelayOutputButton;
use Psr\Http\Message\ResponseInterface;

class RelayOutputButtonTest extends \PHPUnit_Framework_TestCase {
    public function testPress() {
        $transport = $this->getMockBuilder(MegaDeviceTransportInterface::class)->getMock();
        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $response->method('getStatusCode')->willReturn(200);

        $transport
            ->expects($this->once())
            ->method('request')
            ->with(['pt' => 14, 'cmd' => '14:1;p2;14:0'])
            ->willReturn($response);

        $button = new RelayOutputButton(14, $transport);
        $button->press();
    }
}
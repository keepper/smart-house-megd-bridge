<?php
namespace Keepper\SmartHouseMegadBridge\Toggle;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\MegaD\Command\CommandBuilder;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;
use Keepper\SmartHouse\Core\Toggle\Event\SwitchEventSubscriberInterface;
use Keepper\SmartHouse\Core\Toggle\SwitchInterface;
use Keepper\SmartHouse\Core\Toggle\ToggleSwitch;
use Keepper\SmartHouseMegadBridge\MegadPortTrait;
use Keepper\SmartHouseMegadBridge\MegadTransportTrait;

class MegadSwitch extends ToggleSwitch implements SwitchInterface {

    use MegadTransportTrait, MegadPortTrait;

    public function __construct(
        int $portNumber,
        MegaDeviceTransportInterface $transport,
        bool $defaultState = false,
        StateStorageInterface $stateStorage = null,
        string $uuid = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        $this->setTransport($transport);
        $this->setPortNumber($portNumber);

        parent::__construct($uuid ?? 'megad-switch-port-'.$this->portNumber, $defaultState, $stateStorage, $dispatcher);
    }

    /**
     * @inheritdoc
     */
    public function state(): bool {
        $response = $this->transport->request(['pt' => $this->portNumber]);
        $responseHtml = (string) $response->getBody();

        if (!preg_match('/P'.$this->portNumber.'.([ONF]{2,3})/', $responseHtml, $matched)) {
            throw new \Exception('Не удалось разобрать ответ');
        }

        $this->state = $matched[1] == 'ON';
        $this->stateStorage->saveValue($this->uuid(), $this->state);
        return parent::state();
    }

    /**
     * @inheritdoc
     */
    public function turnOn() {
        if ( $this->state ) {
            return;
        }

        $cmdBuilder = new CommandBuilder();
        $cmdBuilder->turnOn(22);

        $response = $this->transport->request([
            'pt' => $this->portNumber,
            'cmd' => $cmdBuilder->getResult()
        ]);

        if ($response->getStatusCode() != 200) {
            throw new \Exception('Ответ не 200');
        }

        $this->state = true;

        $this->stateStorage->saveValue($this->uuid(), $this->state);
        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::CHANGED, [$this->uuid(), $this->state]);
        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::TURNED_ON, [$this->uuid()]);
    }

    /**
     * @inheritdoc
     */
    public function turnOff() {
        if ( !$this->state ) {
            return;
        }

        $cmdBuilder = new CommandBuilder();
        $cmdBuilder->turnOff(22);

        $response = $this->transport->request([
            'pt' => $this->portNumber,
            'cmd' => $cmdBuilder->getResult()
        ]);

        if ($response->getStatusCode() != 200) {
            throw new \Exception('Ответ не 200');
        }

        $this->state = false;
        $this->stateStorage->saveValue($this->uuid(), $this->state);
        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::CHANGED, [$this->uuid(), $this->state]);
        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::TURNED_OFF, [$this->uuid()]);
    }

    /**
     * @inheritdoc
     */
    public function toggle() {
        $cmdBuilder = new CommandBuilder();
        $cmdBuilder->outputToggle(22);

        $response = $this->transport->request([
            'pt' => $this->portNumber,
            'cmd' => $cmdBuilder->getResult()
        ]);

        if ($response->getStatusCode() != 200) {
            throw new \Exception('Ответ не 200');
        }

        $this->state = !$this->state;
        $this->stateStorage->saveValue($this->uuid(), $this->state);

        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::CHANGED, [$this->uuid(), $this->state]);

        if ($this->state) {
            $this->dispatcher->dispatch(SwitchEventSubscriberInterface::TURNED_ON, [$this->uuid()]);
        } else {
            $this->dispatcher->dispatch(SwitchEventSubscriberInterface::TURNED_OFF, [$this->uuid()]);
        }
    }

}
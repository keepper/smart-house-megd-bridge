<?php
namespace Keepper\SmartHouseMegadBridge;

trait MegadPortTrait {
    protected $portNumber;

    protected function setPortNumber(int $portNumber) {
        $this->portNumber = $portNumber;
    }
}
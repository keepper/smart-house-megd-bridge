<?php
namespace Keepper\SmartHouseMegadBridge;

use Keepper\MegaD\Transport\MegaDeviceTransportInterface;

trait MegadTransportTrait {
    /**
     * @var MegaDeviceTransportInterface
     */
    protected $transport;

    protected function setTransport(MegaDeviceTransportInterface $transport) {
        $this->transport = $transport;
    }
}
<?php
namespace Keepper\SmartHouseMegadBridge\Sensor;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\MegaD\Device\MegaDeviceControllerInterface;
use Keepper\SmartHouse\Core\Sensor\AbstractSensor;
use Keepper\SmartHouse\Core\Sensor\SensorInterface;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;

class ControllerTimeSensor extends AbstractSensor implements SensorInterface {

    private $mega;

    public function __construct(
        string $uuid,
        StateStorageInterface $stateStorage = null,
        MegaDeviceControllerInterface $megaDeviceController,
        EventDispatcherInterface $dispatcher = null
    ){
        $this->mega = $megaDeviceController;
        parent::__construct($uuid, $stateStorage, $dispatcher);
    }

    protected function readValue() {
        $dateTime = $this->mega->getDeviceTime();
        return $dateTime->format('Y-m-d H:i:s');
    }
}
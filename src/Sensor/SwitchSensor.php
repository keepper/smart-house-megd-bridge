<?php
namespace Keepper\SmartHouseMegadBridge\Sensor;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Keepper\SmartHouse\Core\Sensor\AbstractDigitalSensor;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;
use Keepper\SmartHouseMegadBridge\MegadPortTrait;
use Keepper\SmartHouseMegadBridge\MegadTransportTrait;

class SwitchSensor extends AbstractDigitalSensor {
    use MegadTransportTrait, MegadPortTrait;

    public function __construct(
        int $portNumber,
        MegaDeviceTransportInterface $transport,
        string $uuid = null,
        StateStorageInterface $stateStorage = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        $this->setTransport($transport);
        $this->setPortNumber($portNumber);
        parent::__construct($uuid ?? 'megad-switch-sensor-'.$portNumber, $stateStorage, $dispatcher);
    }

    protected function readValue() {
        $response = $this->transport->request(['pt' => $this->portNumber]);
        $responseHtml = (string) $response->getBody();

        if (!preg_match('/P'.$this->portNumber.'.([ONF]{2,3})/', $responseHtml, $matched)) {
            throw new \Exception('Не удалось разобрать ответ');
        }

        return $matched[1] == 'ON';
    }
}
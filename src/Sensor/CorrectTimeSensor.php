<?php
namespace Keepper\SmartHouseMegadBridge\Sensor;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\MegaD\Device\MegaDeviceControllerInterface;
use Keepper\SmartHouse\Core\Sensor\AbstractDigitalSensor;
use Keepper\SmartHouse\Core\Sensor\DigitalSensorInterface;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;

class CorrectTimeSensor extends AbstractDigitalSensor implements DigitalSensorInterface {

    private $mega;

    public function __construct(
        string $uuid,
        StateStorageInterface $stateStorage = null,
        MegaDeviceControllerInterface $megaDeviceController,
        EventDispatcherInterface $dispatcher = null
    ){
        $this->mega = $megaDeviceController;
        parent::__construct($uuid, $stateStorage, $dispatcher);
    }

    protected function readValue() {
        $serverTime = new \DateTime('now');
        $controllerTime = $this->mega->getDeviceTime();

        return $serverTime->format('Y-m-d H:i:s') == $controllerTime->format('Y-m-d H:i:s');
    }
}
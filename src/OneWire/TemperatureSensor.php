<?php
namespace Keepper\SmartHouseMegadBridge\OneWire;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Keepper\SmartHouse\Core\Sensor\AbstractSensor;
use Keepper\SmartHouse\Core\Sensor\SensorInterface;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;
use Keepper\SmartHouseMegadBridge\MegadPortTrait;
use Keepper\SmartHouseMegadBridge\MegadTransportTrait;

class TemperatureSensor extends AbstractSensor implements SensorInterface {
    use MegadTransportTrait, MegadPortTrait;

    public function __construct(
        int $portNumber,
        MegaDeviceTransportInterface $transport,
        string $uuid,
        StateStorageInterface $stateStorage = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        $this->setTransport($transport);
        $this->setPortNumber($portNumber);

        parent::__construct('megad-w1-temperature-'.$uuid, $stateStorage, $dispatcher);
    }

    protected function readValue() {
        $response = $this->transport->request(['pt' => $this->portNumber, 'cmd' => 'list']);

        if ($response->getStatusCode() != 200) {
            throw new \Exception('Ответ не 200');
        }

        $body = (string) $response->getBody();
        $sensorUuid = str_replace('megad-w1-temperature-', '', $this->uuid());
        $sensors = explode(';', $body);
        foreach ($sensors as $sensor) {
            $data = explode(':', $sensor);
            if ($data[0] == $sensorUuid) {
                if ( !isset($data[1]) ) {
                    return null;
                }

                return $data[1];
            }
        }

        throw new \Exception('Датчик температуры с указанным uuid не обнаружен');
    }
}
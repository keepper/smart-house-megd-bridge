<?php
namespace Keepper\SmartHouseMegadBridge\OneWire;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Keepper\SmartHouse\Core\Sensor\AbstractDigitalSensor;
use Keepper\SmartHouse\Core\Sensor\DigitalSensorInterface;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;
use Keepper\SmartHouseMegadBridge\MegadPortTrait;
use Keepper\SmartHouseMegadBridge\MegadTransportTrait;

class LabelSensor extends AbstractDigitalSensor implements DigitalSensorInterface {

    use MegadTransportTrait, MegadPortTrait;

    public function __construct(
        int $portNumber,
        MegaDeviceTransportInterface $transport,
        string $uuid,
        StateStorageInterface $stateStorage = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        $this->setTransport($transport);
        $this->setPortNumber($portNumber);
        parent::__construct('megad-w1-label-'.$uuid, $stateStorage, $dispatcher);
    }

    protected function readValue() {
        $response = $this->transport->request(['pt' => $this->portNumber, 'cmd' => 'list']);

        if ($response->getStatusCode() != 200) {
            throw new \Exception('Ответ не 200');
        }

        $body = (string) $response->getBody();
        $sensorUuid = str_replace('megad-w1-label-', '', $this->uuid());
        $sensors = explode(';', $body);
        foreach ($sensors as $sensor) {
            $data = explode(':', $sensor);
            if ($data[0] == $sensorUuid) {
                return true;
            }
        }

        return false;
    }
}
<?php
namespace Keepper\SmartHouseMegadBridge\Output;

use Keepper\MegaD\Device\MegaDeviceControllerInterface;
use Keepper\SmartHouse\Core\Output\OutputSensorInterface;

class WriteTimeSensor implements OutputSensorInterface {

    private $uuid;

    /**
     * @var MegaDeviceControllerInterface
     */
    private $controller;

    public function __construct(
        string $uuid,
        MegaDeviceControllerInterface $controller
    ) {
        $this->uuid = $uuid;
        $this->controller = $controller;
    }

    public function write($value) {
        if ($value instanceof \DateTimeInterface) {
            $date = $value;
        } else if ( is_string($value) ) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
        } else {
            throw new \Exception('Переданное значение для записи не корректно: '.print_r($value, true));
        }

        $this->controller->setTime($date);
    }

    /**
     * @inheritdoc
     */
    public function uuid(): string {
        return $this->uuid;
    }
}
<?php
namespace Keepper\SmartHouseMegadBridge\Button;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\MegaD\Command\CommandBuilder;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Keepper\SmartHouse\Core\Button\Button;
use Keepper\SmartHouse\Core\Button\ButtonInterface;
use Keepper\SmartHouse\Core\Button\PressModeInterface;
use Keepper\SmartHouseMegadBridge\MegadPortTrait;
use Keepper\SmartHouseMegadBridge\MegadTransportTrait;

class RelayOutputButton extends Button implements ButtonInterface {

    use MegadTransportTrait, MegadPortTrait;

    public function __construct(
        int $portNumber,
        MegaDeviceTransportInterface $transport,
        string $uuid = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        $this->setTransport($transport);
        $this->setPortNumber($portNumber);
        parent::__construct($uuid ?? 'relay-button-'.$portNumber, $dispatcher);
    }

    /**
     * @inheritdoc
     */
    public function press(int $mode = PressModeInterface::SINGLE) {
        if ( $mode == PressModeInterface::DOUBLE || $mode == PressModeInterface::LONG ) {
            throw new \Exception('Не поддержииваемый режим нажатия');
        }

        $builder = new CommandBuilder();
        $cmd = $builder
            ->turnOn($this->portNumber)
            ->delay(2)
            ->turnOff($this->portNumber)
            ->getResult();

        $response = $this->transport->request(['pt' => $this->portNumber, 'cmd' => $cmd]);

        if ($response->getStatusCode() != 200) {
            throw new \Exception('Ответ не 200');
        }

        parent::press($mode);
    }
}